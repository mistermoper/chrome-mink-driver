Changelog
=========

## 2.0.1

* Removed behat dependency

## 2.0.0

* Fixed screenshot feature (thanks https://gitlab.com/OmarMoper)

* Extracted behat extension to its own repository

## 1.1.3

* Fixed timeout when checking for the status code of a request served from cache

## 1.1.2

* PHP 5.6 support

* Fixed websocket timeout when visit() was not the first action after start() or reset()

## 1.1.1

* Licensed as MIT

## 1.1.0

* Added support for basic http authentication

* Added support for removing http-only cookies

* Added support for file upload

* Fixed getContent() returning htmldecoded text instead of the outer html as is

## 1.0.1

* Fixed back() and forward() timing out when the page is served from cache.
